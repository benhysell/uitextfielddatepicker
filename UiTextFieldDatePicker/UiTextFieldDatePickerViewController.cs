using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace UiTextFieldDatePicker
{
	public partial class UiTextFieldDatePickerViewController : UIViewController
	{
		/// <summary>
		/// The keyboard observer will show.
		/// </summary>
		private NSObject keyboardObserverWillShow;

		/// <summary>
		/// The keyboard observer will hide.
		/// </summary>
		private NSObject keyboardObserverWillHide;

		public UiTextFieldDatePickerViewController (IntPtr handle) : base (handle)
		{
		}
		
		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
			UnregisterKeyboardNotifications();
			ReleaseDesignerOutlets ();
		}
		
		#region View lifecycle
		
		public override void ViewDidLoad()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
			//setup date picker
			var datePicker = new UIDatePicker();
			datePicker.AutoresizingMask = UIViewAutoresizing.All;
			datePicker.SizeToFit();
			datePicker.AutosizesSubviews = true;

			//setup the done bar above the picker
			var datePickerBar = new UIToolbar();
			datePickerBar.AutoresizingMask = UIViewAutoresizing.All;
			datePickerBar.BarStyle = UIBarStyle.Default;
			datePickerBar.SizeToFit();

			//configure done button
			var doneButton = new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate(object sender, EventArgs e) {
				DateField.Text = datePicker.Date.ToString();
				DateField.ResignFirstResponder();
			} );
			//add done button to bar
			datePickerBar.Items = new UIBarButtonItem[]{doneButton};

			//add picker to input view and setup input accessory view
			DateField.InputView = datePicker;
			DateField.InputAccessoryView = datePickerBar;

			RegisterForKeyboardNotifications();
		}
						
		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear (animated);
		}
		
		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear (animated);
		}
		
		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear (animated);
		}
		
		public override void ViewDidDisappear(bool animated)
		{
			base.ViewDidDisappear (animated);
		}
		
		#endregion

		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
		{
			return UIInterfaceOrientationMask.AllButUpsideDown;
		}

		/// <summary>
		/// Registers for keyboard notifications.
		/// </summary>
		protected virtual void RegisterForKeyboardNotifications ()
		{
			keyboardObserverWillShow = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, KeyboardWillShowNotification);
			keyboardObserverWillHide = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, KeyboardWillHideNotification);
		}

		/// <summary>
		/// Unregisters the keyboard notifications.
		/// </summary>
		protected virtual void UnregisterKeyboardNotifications()
		{
			NSNotificationCenter.DefaultCenter.RemoveObserver(keyboardObserverWillShow);
			NSNotificationCenter.DefaultCenter.RemoveObserver(keyboardObserverWillHide);
		}

		/// <summary>
		/// React to Keyboards the will show notification.
		/// </summary>
		/// <param name='notification'>
		/// Notification.
		/// </param>
		protected virtual void KeyboardWillShowNotification (NSNotification notification)
		{
			UIView activeView = KeyboardGetActiveView();
			if (activeView == null)
				return;
			
			UIScrollView scrollView = activeView.FindSuperviewOfType(this.View, typeof(UIScrollView)) as UIScrollView;
			if (scrollView == null)
				return;
			
			RectangleF keyboardBounds = UIKeyboard.FrameBeginFromNotification(notification);
			
			UIEdgeInsets contentInsets = new UIEdgeInsets(0.0f, 0.0f, keyboardBounds.Size.Height, 0.0f);
			scrollView.ContentInset = contentInsets;
			scrollView.ScrollIndicatorInsets = contentInsets;
			
			// If activeField is hidden by keyboard, scroll it so it's visible
			RectangleF viewRectAboveKeyboard = new RectangleF(this.View.Frame.Location, new SizeF(this.View.Frame.Width, this.View.Frame.Size.Height - keyboardBounds.Size.Height));
			
			RectangleF activeFieldAbsoluteFrame = activeView.Superview.ConvertRectToView(activeView.Frame, this.View);
			// activeFieldAbsoluteFrame is relative to this.View so does not include any scrollView.ContentOffset
			
			// Check if the activeField will be partially or entirely covered by the keyboard
			if (!viewRectAboveKeyboard.Contains(activeFieldAbsoluteFrame))
			{
				// Scroll to the activeField Y position + activeField.Height + current scrollView.ContentOffset.Y - the keyboard Height
				PointF scrollPoint = new PointF(0.0f, activeFieldAbsoluteFrame.Location.Y + activeFieldAbsoluteFrame.Height + scrollView.ContentOffset.Y - viewRectAboveKeyboard.Height);
				scrollView.SetContentOffset(scrollPoint, true);
			}
		}

		protected virtual UIView KeyboardGetActiveView()
		{
			return this.View.FindFirstResponder();
		}

		/// <summary>
		/// Keyboards the will hide notification.
		/// </summary>
		/// <param name='notification'>
		/// Notification.
		/// </param>
		protected virtual void KeyboardWillHideNotification (NSNotification notification)
		{
			UIView activeView = KeyboardGetActiveView();
			if (activeView == null)
				return;
			
			UIScrollView scrollView = activeView.FindSuperviewOfType (this.View, typeof(UIScrollView)) as UIScrollView;
			if (scrollView == null)
				return;
			
			// Reset the content inset of the scrollView and animate using the current keyboard animation duration
			double animationDuration = UIKeyboard.AnimationDurationFromNotification(notification);
			UIEdgeInsets contentInsets = new UIEdgeInsets(0.0f, 0.0f, 0.0f, 0.0f);
			UIView.Animate(animationDuration, delegate{
				scrollView.ContentInset = contentInsets;
				scrollView.ScrollIndicatorInsets = contentInsets;
			});
		}
	}

	public static class ViewExtensions
	{
	public static UIView FindFirstResponder (this UIView view)
	{
		if (view.IsFirstResponder)
		{
			return view;
		}
		foreach (UIView subView in view.Subviews) {
			var firstResponder = subView.FindFirstResponder();
			if (firstResponder != null)
				return firstResponder;
		}
		return null;
	}

	public static UIView FindSuperviewOfType(this UIView view, UIView stopAt, Type type)
	{
		if (view.Superview != null)
		{
			if (type.IsAssignableFrom(view.Superview.GetType()))
			{
				return view.Superview;
			}
			
			if (view.Superview != stopAt)
				return view.Superview.FindSuperviewOfType(stopAt, type);
		}
		
		return null;
	}
	}
}

